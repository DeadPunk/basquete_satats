import streamlit as st
import pandas as pd 
import numpy as np 
import pydeck as pdk 
import plotly.express as px
from PIL import Image
import plotly.graph_objects as go
import matplotlib.pyplot as plt


DATA_URL = ('C:/Users/Dinopc/Documents/basquete/cestas.csv')


@st.cache(persist=True)

def load_data(nrows):
	data = pd.read_csv(DATA_URL, nrows=nrows)
	#data.dropna(subset=['LATITUDE', 'LONGITUDE'], inplace=True)
	#lowercase = lambda x: str(x).lower()
	#data.rename(lowercase, axis='columns', inplace=True)
	#data.rename(columns={'crash_date_crash_time': 'date/time'}, inplace=True)

	return data 

data = load_data(13)

image = Image.open('C:/Users/Dinopc/Documents/basquete/basketball1.jpg')
st.image(image)


st.header('Estatísticas do ano de 2020')

st.markdown('### Comparativo mensal')
#hist = np.histogram(data['Meses'], bins=60, range=(0, 60)) [0]
#chart_datapd.DataFrame({'minute': range(60), 'crashes': hist}) = 
fig = px.bar(data, x = 'Meses', y = ['2pts', '3pts', 'LL'], height=450)
st.write(fig) 

data2 = pd.DataFrame({'cestas': [297, 308, 283]})


st.markdown('### % Cestas')
fig2 = px.pie(data2, values='cestas', names=['3pts', '2pts', 'LL'])
st.write(fig2)




st.markdown('### Locais com maior numero de cestas')

img = plt.imread('C:/Users/Dinopc/Documents/basquete/quadra.jpg')
fig3, ax = plt.subplots()
ax.imshow(img)

fig3, ax = plt.subplots()
x = [50, 150, 100, 100, 50, 150, 165, 50, 56, 50, 55, 65, 120, 45, 70, 70, 150, 145, 100, 110, 99, 80]
y = [150, 200, 250, 100, 50, 45, 145, 165, 170, 200, 210, 100, 50, 135, 90, 40, 100, 230, 165, 150, 175, 132]
ax.imshow(img, extent=[0,400,0,300])
ax.scatter(x,y, color='firebrick')

#ext = [0.0, 10.0, 0.00, 1.25]
#fig3 = px.imshow(img)
st.write(fig3)
